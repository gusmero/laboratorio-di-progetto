package edu.AG.LaboratorioDiProgettazione.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "menu")
public class Menu {
	
  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  private String id;

  private String titolo;

  
  private String descrizione;

 
  private String fileName;

  public Menu(String titolo , String descrizione , String fileName){
    this.titolo=titolo;
    this.descrizione=descrizione;
    this.fileName=fileName;
  }

  public Menu(){}

  public String getId(){
    return id;
  }

  public String getTitolo(){
    return titolo;
  }

  public void setTitolo(String titolo){
    this.titolo=titolo;
  }

  public String getDescrizione(){
    return this.descrizione;
  }

  public void setPrimi(String descrizione){
    this.descrizione=descrizione;
  }

  public String getFileName(){
    return this.fileName;
  }

  public void setSecondi(String fileName){
    this.fileName=fileName;
  }

}