package edu.AG.LaboratorioDiProgettazione.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Entity
@Table(name = "accounts")
public class Account {

//	@Autowired
//	@Transient // per permettere di ignorare il campo
//	BCryptPasswordEncoder passwordEncoder;

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

	@Column(name = "user_name")
    private String userName;

    @Column(name = "password")
    private String password;
    
    
    @Column(name = "role")
    private String role;
    
    public Account(){}
    
	public Account(String userName, String password, String role) {
		super();
		BCryptPasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
		this.userName = userName;
		//this.password=password;
		setPassword(passwordEncoder.encode(password));
		System.out.println("the password encrypted is: "+ this.password);
		this.role = role;
	}

//	public BCryptPasswordEncoder getPasswordEncoder() {
//		return passwordEncoder;
//	}
//
//	public void setPasswordEncoder(BCryptPasswordEncoder passwordEncoder) {
//		this.passwordEncoder = passwordEncoder;
//	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}