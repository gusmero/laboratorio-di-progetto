package edu.AG.LaboratorioDiProgettazione.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import edu.AG.LaboratorioDiProgettazione.domain.Menu;
import edu.AG.LaboratorioDiProgettazione.repository.MenuRepository;

@Service
public class MenuService {

	@Autowired
	private  MenuRepository MenuRepository;



	
	/**Used for saving menu object thanks the repository and saving the multipartfile 
	 * thanks to saveFile, the file is save into  ./src/main/resource/static/images/menu
	 * 
	 * @param file
	 * @param titolo
	 * @param descrizione
	 * @return Menu
	 * @throws IOException
	 */
	public Menu store(MultipartFile file, String titolo,String descrizione) throws IOException {
		String fileName = file.getOriginalFilename();
		Menu menu = new Menu(titolo, descrizione, fileName);
		String uploadDir = "src/main/resources/static/images/menus";
		saveFile(uploadDir, fileName, file);
		return MenuRepository.save(menu);
	}




	public List<Menu> getMenus() {
		List<Menu> menus =new ArrayList<>();
		for (Menu menu: MenuRepository.findAll()) {
			menus.add(menu);
		}
		if(menus.isEmpty()) {
			System.out.println("findall non recupera niente");
		}
		System.out.println("findall contiene qualcosa "+ menus.size());
		return menus;
	}


	public void removeAll() {
		MenuRepository.deleteAll();
	}



	
	/** Used for deleting the Menu object present into DB and deleting the images associated
	 * @param id
	 */
	public void deleteMenu(String id) {
		Menu menu=getFile(id);
		//DELETE ELEMENT FROM FOLDER
		Path imagesPath = Paths.get("src/main/resources/static/images/menus/" 
					+ menu.getFileName());
		try {
			Files.delete(imagesPath);
			System.out.println("File "
					+ imagesPath.toAbsolutePath().toString()
					+ " successfully removed");
		} catch (IOException e) {
			System.err.println("Unable to delete "
					+ imagesPath.toAbsolutePath().toString()
					+ " due to...");
			e.printStackTrace();
		}
		MenuRepository.deleteById(id);
	}


	public Menu getFile(String id) {
		return MenuRepository.findById(id).get();
	}
	
	
	

	
	/** 
	 * This method permit to save a multipart file into static folder
	 * @param uploadDir
	 * @param fileName
	 * @param multipartFile
	 * @throws IOException
	 * 
	 */
	public static void saveFile(String uploadDir, String fileName,
        MultipartFile multipartFile) throws IOException {
        Path uploadPath = Paths.get(uploadDir);
         
        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
         
        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {        
            throw new IOException("Could not save image file: " + fileName, ioe);
        }      
    }











	

}