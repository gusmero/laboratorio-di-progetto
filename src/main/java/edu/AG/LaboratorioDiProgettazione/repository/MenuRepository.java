package edu.AG.LaboratorioDiProgettazione.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import edu.AG.LaboratorioDiProgettazione.domain.Menu;

public interface MenuRepository extends JpaRepository<Menu, String>{
	

}