package edu.AG.LaboratorioDiProgettazione.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import edu.AG.LaboratorioDiProgettazione.domain.Account;

public interface AccountRepository extends JpaRepository<Account, Long>{
	
	boolean existsByUserName(String nome);
	Account findByUserName(String nome);

}
