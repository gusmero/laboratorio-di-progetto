package edu.AG.LaboratorioDiProgettazione.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import edu.AG.LaboratorioDiProgettazione.service.MenuService;
import edu.AG.LaboratorioDiProgettazione.domain.*;
import edu.AG.LaboratorioDiProgettazione.repository.*;


@Controller
@RequestMapping("/")
public class IndexController {
	
	
	@Autowired
    MenuService menuService;

	@Autowired
	AccountRepository accountRepo;
	

	@GetMapping("")
	public String chiamataIndex(Model model) {
		model.addAttribute("menus", menuService.getMenus());
		return "index";
	}
	
	@GetMapping("/login")
	public String chiamataLogin() {
		return "login.html";
	}

	@GetMapping("/registrazione")
	public String chiamataRegistration() {
		return "registrazione";
	}

	@PostMapping("/registrazione/newAccount")
	public String newAccount(@RequestParam("userName") String userName,
							@RequestParam("password") String password ) {
		Account account=new Account(userName, password , "user");	
		accountRepo.save(account);			
		return "redirect:/login";
	}

	
}
