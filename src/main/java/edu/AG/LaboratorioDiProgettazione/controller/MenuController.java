package edu.AG.LaboratorioDiProgettazione.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.AG.LaboratorioDiProgettazione.domain.Menu;
import edu.AG.LaboratorioDiProgettazione.service.MenuService;

@Controller
public class MenuController {

    @Autowired
    MenuService menuService;

    String message;


  
  @RequestMapping("/manageMenu")
	public String chiamataGestisciMenu(Model model) {
    model.addAttribute("menus", menuService.getMenus());
		return "manageMenu";
	}
  
    
    @PostMapping("/manageMenu/upload")
    public  String uploadFile(@RequestParam("file") MultipartFile file,
    @RequestParam("titolo") String titolo,
    @RequestParam("descrizione") String descrizione ) {
      if (file.isEmpty()) {
          message= "devi aggiungere un file";
          return "redirect:/manageMenu";
      }
        try {
          menuService.store(file, titolo, descrizione );
      } catch (IOException e) {
          e.printStackTrace();
      }
      message="menu aggiunto";
      return "redirect:/manageMenu";
    }



    @GetMapping("/manageMenu/delete/{id}")
  public String deleteFile(@PathVariable String id, Model model) {
    menuService.deleteMenu(id);
    return "redirect:/manageMenu";
  }
  
  
  
  @RequestMapping("/manageMenu/deleteAll")
  public String deletAll(Model model) {
	  menuService.removeAll();
      return "redirect:/manageMenu";
  }
  
  @GetMapping("/manageMenu/getMenuFoto/{id}")
  public String getFile(@PathVariable String id, Model model , RedirectAttributes redirectAttributes) {
    Menu menu=menuService.getFile(id);
    redirectAttributes.addFlashAttribute("menuFoto", "/src/main/resources/static/images/menus/"+menu.getFileName());
    return "redirect:/manageMenu";
  }

  public String initTest(){
    return "Unit test";
  }
    
}
