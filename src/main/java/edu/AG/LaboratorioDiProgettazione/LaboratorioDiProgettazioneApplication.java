package edu.AG.LaboratorioDiProgettazione;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LaboratorioDiProgettazioneApplication {

	public static void main(String[] args) {
		SpringApplication.run(LaboratorioDiProgettazioneApplication.class, args);
	}

}
