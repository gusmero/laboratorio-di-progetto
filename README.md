# Progetto LandingPage


# Introduzione

Quosto progetto si pone l'obbiettivo di creare un sistema con caratteristiche cloud-native utilizzabile per la creazione e la sua gestione d'informazione publicitaria , e di possibili servizi erogabili per un pubblico di possibili clienti di una azienda di ristorazione.



# Cloud-native application
La nostra applicazione presenterà le caratteristiche cloud-native.
Quindi utilizzeremo il processo di sviluppo DevOps , specificando pipelines e stages che permettano di ottenere l'obbiettivo di una continua Integrazione e una distribuzione continua [CI/CD](#cicd). Il sistema rappresenta un'architettura MONOLITICA la quale verra poi containerizzata tramite [Docker](https://www.docker.com/) per la loro distribuzione in fase di deploying.

 


# Servizi esterni
All'interno del sistema per migliorare il servizio offerto dall'applicazione abbiamo deciso di aggiungere due servizi esterni , i quali vengono contati dal nostro sistema tramite API. Il primo servizio è quello di [google Maps](https://mapsplatform.google.com/) che ci permette di aggiungere la mappa di google nella nostra UX , e di conseguenza di permettere ai clienti di fare partire una navigazione stradale dal telefono.
Il secondo servizio è quello offerto da [OpenWeatherApi](https://openweathermap.org/api) che ci ha permesso di mostrare nella nostra UX le previsione meteo nella zona del ristorante.


# Architettura software


# Git Branch

- master (commit comment only 0.0)
- integration  (commit comment type test pass)
- development (commit comment : bug fixed , future complete) can only start from integration
- "particularFeatures"  (can only sart from development)





<a name="DevOps"></a> 
# DevOps
![DevOps Tools](/Readme-images/DevOps-Tools.JPG)




<a name="cicd"></a> 
# CI/CD Pipelines
Di seguito si mostrano gli stage/job scelti per la nostra pipeline(CI/CD):

- **Build**
- **Verify**
- **Unit-test**
- **Integration-test**
- **Package**
- **Release**
- **Deploy**



# Definizioni Globali

Se non viene specificata un'immagine all'interno di ogni stage, viene usata di default quella di maven:3.6.3

```maven
image: maven:3.6.3-jdk-11
```
Variabili di ambiente globali disponibili all'interno di ogni job:

```maven
variables:
MAVEN_CLI_OPTS: "--strict-checksums --threads 1C --batch-mode"
MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
DOCKER_HOST: "tcp://docker:2375"
DOCKER_DRIVER: overlay2
IMAGE_NAME: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
```



# Build

La build del progetto viene realizzata sfruttando l'apposito goal **compile** di Maven:

```maven
mvn $MAVEN_CLI_OPTS $MAVEN_OPTS compile
```




# Verify

In questo secondo stage ci affidiamo a due plugins ([Checkstyle](https://checkstyle.sourceforge.io/) e [Spotbugs](https://spotbugs.github.io/)) per l'analisi di stile del codice e per l'ispezione di eventuali bug o code smells, sfruttando gli appositi goals in Maven.

Uso di Checkstyle con goal check:
```maven
mvn $MAVEN_CLI_OPTS $MAVEN_OPTS checkstyle:check
```
Uso di Spotbugs con goal check:
```maven
mvn $MAVEN_CLI_OPTS $MAVEN_OPTS spotbugs:check
```

Sfruttiamo in questo passaggio una cache per permettere la comunicazione tra jobs diversi, per natura indipendenti tra loro, garantendo il **save-restore** di quest'ultima in step successivi, questo non solo migliora l'efficenza del singolo job ma permette di non eseguire nuovamente istruzioni che sarebbero ridondanti in questo stage.

```maven
 cache:
    key: "$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG"
    paths:
      - .m2/repository/
      - target/

```


# Test

Per quanto riguarda la fase di test abbiamo deciso di separare le tipologie da implementare in questo assignment, come richiesto nelle specifiche del progetto.
Ci siamo serviti di due plugin per automatizzare il processo di testing:
- [Surefire](https://maven.apache.org/surefire/maven-surefire-plugin/): per i test di unità.
- [Failsafe](https://maven.apache.org/surefire/maven-failsafe-plugin/): per i test di integrazione.

Di seguito proponiamo la spiegazione per queste categorie di test.

## Unit-test

Un test di unità si pone come obiettivo quello di verificare il corretto funzionamento di un solo modulo all'interno di un sistema, ad esempio un singolo metodo o funzione, nel nostro caso, data la libertà nella scelta del metodo da testare, abbiamo creato una semplice classe HelloControllerTest.java che si interfacciasse con l'apposito Controller (HelloController.java) per recuperare una stringa (attraverso il metodo: String unitTest()) ed effettuare l'assert su quest'ultima.

Visto che il goal **verify** di Maven esegue implicitamente anche la fase di test (sia unit che integration) e di package abbiamo deciso di sfruttare quest'ultimo per adempire al nostro scopo, posponendo il parametro "-DskipITs=true" al comando, per **non** eseguire anche i test di integrazione.

```maven
mvn $MAVEN_CLI_OPTS $MAVEN_OPTS verify -DskipITs=true
```
## Integration-test

In un test di integrazione abbiamo come scopo quello di testare il coordinamento tra due o più (macro)componenti, nel nostro caso, tra database postgresql e applicazione.
All'interno della classe HelloControllerIT.java viene prima stabilita una connessione col database, successivamente vengono inseriti nel database due accessi e si controlla tramite assert che questi siano effettivamente il numero che ci aspettavamo.

Come anticipato per i test di unità abbiamo lo stesso goal **verify** ma stavolta seguito dal parametro "-DskipUTs=true" per **non** eseguire i test di unità in questo stage.
```maven
mvn $MAVEN_CLI_OPTS $MAVEN_OPTS verify -DskipUTs=true
```


# Package

Nello stage di package tutte i file/librerie/dipendenze utilizzate dall'applicazione vengono compresse in un unico file con estensione .jar per facilitarne la distribuzione, in quanto un unico file "compresso" richiede un minore sforzo per essere trasferito sulla rete ed elaborato da un calcolatore.
La compressione non altera la struttura dei file stessi e inoltre slega l'applicazione dalla piattaforma nativa a patto di sfruttare una JVM per la sua esecuzione.
In maven questo viene realizzato attraverso l'apposito goal **package**:

```maven
mvn $MAVEN_CLI_OPTS $MAVEN_OPTS -DskipTests clean package spring-boot:repackage
```
Viene quindi prodotto un unico file .jar all'interno della cartella target pronto per essere rilasciato e distribuito.

```maven
paths:
    - target/*.jar
```


<a name="Containerized"></a>
# Release

Durante lo stage di release ci affidiamo a Docker, in particolare a [Docker Engine](https://docs.docker.com/engine/) per la creazione automatica di un'immagine (sanpietro-image) a partire dalle istruzioni contenute nel Dockerfile , l'immagine viene poi pushata nel repository wyst54rhrb pubblico.
All'inetrno del Dockerfile viene definito come ENTRYPOINT il comando 
```java
java -jar "LandingPage-0.0.1-SNAPSHOT.jar"
```
per eseguire l'applicativo all'avvio del conteiner, tramite .jar che viene ottenuto nello stage di package , e il comando EXPOSE per esporre la porta 2222 all'inetrno del container.
Grazie ai servizi di virtualizzazione a livello di sistema operativo offerti da Docker (Platform as a Service) siamo in grado di avere un container per la nostra applicazione che potrà essere runnato su qualsiasi server.

Connessione al GitLab Container Registry:
```docker
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker build -t $IMAGE_NAME .
docker push $IMAGE_NAME
```



# Deploy on Debian Azure Virtual Machine

Infine, per quest'ultima fase sfruttiamo il servizio di cloud platforming (PaaS) [Azure](https://portal.azure.com/), in cui è installato il sistema open-source [Kubernetes](https://kubernetes.io/) per l'automazione del deployment , scaling e il management di un applicazione contenerizzata.
Con questi due strumenti otteniamo così una configurazione di continuos deployment che risulta pronta a ricevere il deploy tramite processo DevOps.

Lo stage di deploy viene organizzato con una prima connessione alla macchina remota tramite connessione ssh; abbiamo impostato la nostra chiave pubblica RSA sul server e privata sul repository. Disabilitiamo lo StrictHostKeyChecking per fare in modo che venga accettato anche il nostro host.

Dopo aver effettuato la connessione alla macchina remota, ci spostiamo nela cartella specifica e pulliamo il progetto dal repository, nel quale è presente il file YAML di configurazione per il servizio e di deployment necessari.
Dopo aver lanciato l'esecuzione del servizio terminiamo la connessione con la macchina remota.



## Deploy Azure App Service
E' disponibile una seconda modalità di deploy tramite Azure app service , che permette di recuperare la nostra immagine da DokerHub ed eseguirla all'interno di un container Azure , l'App Service è impostato per recuperare il tag 1.1. Altri modi alternativi di deploy in esso possono essere attraverso visual studio code.
Abbiamo dovuto definire inoltre un db sempre in azure per la connessione.
DA VEDERE

 - [ ] utilizzo di Azure function
 - [ ] la possibilità di lavorare con microservizi
 - [ ] lavorare con continuos monitoring (Application of Insight)
 - [ ] scale in/out automatico utilizzando repliche


```sh
spring.datasource.url=jdbc:sqlserver://first-server-01.database.windows.net:1433;database=landingpagedb;user=user@first-server-01;password=Passw0rd;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;
```
Il link da cui è possibile vedere l'applicazione è https://laboratoriodiprogetto.azurewebsites.net/

Al momento non capisco come si colleghi al db.

### Continuos Monitoring with app Service
Using Azure app Service you can enable the monitoring with Application Insight.



## Deploy on Heroku
In this section i will describe a possible deploying configuration for push the entire repository into [heroku](https://dashboard.heroku.com/apps). Heroku is a Platform as Service usefull for pushing webb application with no cost. The application as a public domain that can be achived through this [link](https://laboratorio-di-progettazione.herokuapp.com/). For the db connection i use ClearDB.
In the following i will list all necesary for configure this deploy:
* create a heroku app
* add ClearMysqlDB
* add deployment stage like below
* add two Gitlab CI/CD variable $HEROKU_APP(application name) $HEROKU_API_KEY(achived on Account Setting , ApiKey) 
* add procfile file containing web: java -jar target/LaboratorioDiProgettazione-0.0.1-SNAPSHOT.jar , (started command in heroku)
* add a system.properties file containing java.runtime.version=11
* modified application.properties 
* MANCA aggiungere il primo amministratore nel db con una connessione da workbench semplice , codificare la password con bycrypt prima dell'insert se non il match codificato non avviene.

```properties
# HEROKU DEPLOYMENT
#server.port for heroku application 
#username=b670d597ac1ac2
#password=b320d0ea
#host=eu-cdbr-west-02.cleardb.net
#database=heroku_6e3583fb567df20
#porta=3306
server.port=${PORT:8080}
spring.datasource.url=jdbc:mysql://b670d597ac1ac2:b320d0ea@eu-cdbr-west-02.cleardb.net/heroku_6e3583fb567df20?reconnect=true

```

```yaml
#DEPLOY 
deploy:
  stage: deploy
  image: ruby:2.6
  script:
    - echo "Deploying application..."
    - apt-get update -qy
    - apt-get install -y ruby-dev
    - gem install dpl
    - dpl --provider=heroku --app=$HEROKU_APP --api-key=$HEROKU_API_KEY
    - echo "Deploying completed..."
  only:
    - master
```







# Problemi Riscontrati

Durante lo stage di **deploying** abbiamo riscontrato la necessità di valutare delle mosse per gestire la dipendenza del microservizio con il DataBase Mysql utilizzato , per questo motivo abbiamo aggiunto un comando che permette di valutare lo stato del mysql-server-pod prima dell'esecuzione del file di configurazione del microservizio.



ssh -i C:\Users\andre\Documents\WorkSpace\SSHkey\LaboratorioDiProgettazione_key.pem gusmero@20.107.36.11


ssh -i C:\Download\Progettazione_key.pem azureuser@20.79.217.250


