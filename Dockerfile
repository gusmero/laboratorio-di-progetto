########## Dockerfile #######################
##### utilziamo la jdk-11 come usata sulla nostra applicazione spring #######
FROM openjdk:11-jre

COPY  /target ./
WORKDIR ./

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "LaboratorioDiProgettazione-0.0.1-SNAPSHOT.jar"]

######################################################################

